## Spring of Emails

### Introduction

Spring of Emails application collects dataset via rest endpoint. App produces the email datas to Kafka queue. There are two Kafka topics `emails` and `urls`. The `emails` topic has multiple partitions(eg: 6), so that multiple consumer threads can listen independently without any order. The `urls` topic can be consumed by only single consumer and that prevents multiple reads for the same url. The `urls` consumer produces to `emails` topic , if there are any valid emails in the fetched data.

External data fetching method is configurable. Currently, there is a retry backoff configuration with max attempt 3 and duration as 1 second. 

Incoming batch datas are stored in distributed in-memory cache map. After five minutes, data will be persisted in PostgreSQL database. So, the datas have eventual consistency in terms of database records. The REST endpoints serve the persisted datas from the the database, so it allows application to serves the data that is successfully persisted (Original requirement: Count and persist how many times an email is encountered, in five minutes batches).
`That case can born a discussion about serving datas. Should we follow that way or Should we serve the processing datas in five minute timeframe from the cache?`    


This is designed for a POC. Architectural decision can be discussed for further requirements or application's working principle. External dependencies such as PostgreSQL and Kafka setups should be configured later according to host resources.

### Runnnig the aplication

Depended PostgreSQL and Kafka services should be running, project contains a `docker-compose.yml` file for that purpose:

```
docker compose up -d
```

Building and starting the `Spring of Emails` application:

```
mvn clean install
java -jar target/spring-of-emails-0.0.1-SNAPSHOT.jar
```

`application.yml` properties file is configurable by environment variables. 

Application can be run as multiple instances. You can give different `-Dserver.port=PORT` as JVM parameter while running multiple instances. 

### REST API

You can reach the REST API documentation via `/swagger-ui.html` path.

|                   URL                   | Method |          Remarks       |
|-----------------------------------------|--------|------------------------|
|`https://localhost:8080/emails`           | GET    | get all emails          |
|`https://localhost:8080/emails/{email}`         | GET    | get single email details             |
|`https://localhost:8080/emails`           | POST   | create an email          |
|`https://localhost:8080/emails`         | PUT    | update an email        |
|`https://localhost:8080/emails`       | DELETE    | delete an email           |
|`https://localhost:8083/data/feeder`         | GET   | creates dummy emails and urls        |
|`https://localhost:8083/data/collect`    | POST   | entrypoint to collecting batch data as dataset  |