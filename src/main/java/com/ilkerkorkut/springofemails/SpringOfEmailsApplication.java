package com.ilkerkorkut.springofemails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@EnableConfigurationProperties
@SpringBootApplication
public class SpringOfEmailsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringOfEmailsApplication.class, args);
    }

}
