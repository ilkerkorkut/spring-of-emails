package com.ilkerkorkut.springofemails.util;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtil {

    private static String VALID_EMAIL_REGEX_PATTERN = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@(comeon\\.com|cherry\\.se)$";

    public static boolean isValidEmail(String email) {
        if (Objects.isNull(email)) {
            return false;
        }
        Pattern pattern = Pattern.compile(VALID_EMAIL_REGEX_PATTERN, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidUrl(String url){
        return Objects.nonNull(url) && !"".equals(url);
    }
}
