package com.ilkerkorkut.springofemails.rest;

import com.ilkerkorkut.springofemails.model.Dataset;
import com.ilkerkorkut.springofemails.service.DatasetProducerService;
import com.ilkerkorkut.springofemails.service.DummyDataService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class DataController {

    private final DummyDataService dummyDataService;

    private final DatasetProducerService datasetProducerService;

    @GetMapping(value = "/data/feeder", produces = MediaType.APPLICATION_XML_VALUE)
    public Dataset bulkDataFeeder() {
        return dummyDataService.createDummyData();
    }

    @PostMapping(value = "/data/collect", consumes = MediaType.APPLICATION_XML_VALUE)
    public String dataSetCollector(@RequestBody Dataset dataset) {
        datasetProducerService.produceDataset(dataset);
        return "Dataset is in the queue, will be processed in a short time.";
    }
}
