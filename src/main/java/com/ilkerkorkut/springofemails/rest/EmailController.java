package com.ilkerkorkut.springofemails.rest;

import com.ilkerkorkut.springofemails.model.request.EmailRequest;
import com.ilkerkorkut.springofemails.model.response.EmailListResponse;
import com.ilkerkorkut.springofemails.model.response.EmailResponse;
import com.ilkerkorkut.springofemails.service.EmailService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class EmailController {

    private final EmailService emailService;

    @GetMapping(value = "/emails", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE})
    public EmailListResponse emailList() {
        return emailService.getEmails();
    }

    @GetMapping(value = "/emails/{email}")
    public EmailResponse readEmail(@PathVariable String email) {
        return emailService.getEmail(email);
    }

    @PostMapping(value = "/emails", consumes = MediaType.APPLICATION_XML_VALUE)
    public void createEmail(@RequestBody EmailRequest emailRequest) {
        emailService.createEmail(emailRequest);
    }

    @PutMapping(value = "/emails", consumes = MediaType.APPLICATION_XML_VALUE)
    public void updateEmail(@RequestBody EmailRequest emailRequest) {
        emailService.updateEmail(emailRequest);
    }

    @DeleteMapping(value = "/emails", consumes = MediaType.APPLICATION_XML_VALUE)
    public void deleteEmail(@RequestBody EmailRequest emailRequest) {
        emailService.deleteEmail(emailRequest);
    }
}
