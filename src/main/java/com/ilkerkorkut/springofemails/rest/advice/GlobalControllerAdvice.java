package com.ilkerkorkut.springofemails.rest.advice;

import com.ilkerkorkut.springofemails.model.exception.GenericHttpException;
import com.ilkerkorkut.springofemails.model.response.ErrorResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalControllerAdvice {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception exception) {
        log.error("Generic exception occurred.", exception);
        ErrorResponse errorResponse = ErrorResponse.builder()
                .exception(exception.getClass().getName())
                .timestamp(System.currentTimeMillis())
                .errors(Collections.singletonList("An error occurred."))
                .build();
        return new ResponseEntity<>(errorResponse, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(GenericHttpException.class)
    public ResponseEntity<ErrorResponse> handleGenericHttpException(GenericHttpException exception) {
        final String defaultMessage = "GenericHttpException occured.";
        final ErrorResponse errorResponse = ErrorResponse.builder()
                .exception("GenericHttpException")
                .error(exception.getKey())
                .timestamp(System.currentTimeMillis())
                .build();
        log.error(defaultMessage, exception);
        return new ResponseEntity<>(errorResponse, exception.getHttpStatus());
    }
}
