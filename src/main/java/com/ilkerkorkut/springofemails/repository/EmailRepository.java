package com.ilkerkorkut.springofemails.repository;

import com.ilkerkorkut.springofemails.persistence.EmailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmailRepository extends JpaRepository<EmailEntity, Long> {

    Optional<EmailEntity> findByValue(String value);
}
