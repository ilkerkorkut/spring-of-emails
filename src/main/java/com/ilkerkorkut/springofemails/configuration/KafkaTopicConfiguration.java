package com.ilkerkorkut.springofemails.configuration;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class KafkaTopicConfiguration {

    @Value(value = "${kafka.bootstrap-servers}")
    private List<String> bootstrapAddress;

    @Value("${kafka.emails-topic}")
    private String emailsTopic;

    @Value("${kafka.urls-topic}")
    private String urlsTopic;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic emailsTopic() {
        return new NewTopic(emailsTopic, 6, (short) 1);
    }

    @Bean
    public NewTopic urlsTopic() {
        return new NewTopic(urlsTopic, 1, (short) 1);
    }
}
