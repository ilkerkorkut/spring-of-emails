package com.ilkerkorkut.springofemails.configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.context.SpringManagedContext;
import com.ilkerkorkut.springofemails.model.Constants;
import com.ilkerkorkut.springofemails.service.EmailCacheStoreService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfiguration {

    @Value("${data.persistence.delaySeconds:300}")
    private Integer dataPersistenceDelaySeconds;

    @Bean
    public SpringManagedContext managedContext() {
        return new SpringManagedContext();
    }

    @Bean
    public HazelcastInstance hazelcastInstance() {
        Config config = new Config();
        config.setManagedContext(managedContext());

        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setClassName(EmailCacheStoreService.class.getName()).setEnabled(true);
        mapStoreConfig.setWriteDelaySeconds(dataPersistenceDelaySeconds);

        MapConfig mapConfig = new MapConfig();
        mapConfig.setName(Constants.EMAILS_MAP_NAME);
        mapConfig.setMapStoreConfig(mapStoreConfig);

        config.addMapConfig(mapConfig);

        return Hazelcast.newHazelcastInstance(config);
    }
}
