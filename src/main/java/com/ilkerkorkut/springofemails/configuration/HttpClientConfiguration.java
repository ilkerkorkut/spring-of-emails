package com.ilkerkorkut.springofemails.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class HttpClientConfiguration {

    @Value("${http.client.connection.timeout:10000}")
    private Integer connectionTimeoutMillis;

    @Value("${http.client.response.timeout:10000}")
    private Integer responseTimeoutMillis;

    @Value("${http.client.read.timeout:10000}")
    private Integer readTimeoutMillis;

    @Value("${http.client.write.timeout:10000}")
    private Integer writeTimeoutMillis;
}
