package com.ilkerkorkut.springofemails.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.ilkerkorkut.springofemails.converter.EmailEntityConverter;
import com.ilkerkorkut.springofemails.model.Constants;
import com.ilkerkorkut.springofemails.model.exception.GenericHttpException;
import com.ilkerkorkut.springofemails.model.request.EmailRequest;
import com.ilkerkorkut.springofemails.model.response.EmailListResponse;
import com.ilkerkorkut.springofemails.model.response.EmailResponse;
import com.ilkerkorkut.springofemails.persistence.EmailEntity;
import com.ilkerkorkut.springofemails.repository.EmailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class EmailService {

    private final EmailRepository emailRepository;
    private final EmailEntityConverter emailEntityConverter;
    private final HazelcastInstance hazelcastInstance;

    public void addToCache(String email) {
        IMap<String, Integer> emailsMap = hazelcastInstance.getMap(Constants.EMAILS_MAP_NAME);
        Integer count = emailsMap.get(email);
        if (Objects.isNull(count)) {
            emailsMap.put(email, 1);
        } else {
            emailsMap.put(email, count + 1);
        }
    }

    public EmailListResponse getEmails() {
        List<EmailEntity> emailEntityList = emailRepository.findAll();
        return EmailListResponse.builder()
                .emails(emailEntityList.stream()
                        .map(emailEntityConverter::convert)
                        .collect(Collectors.toList()))
                .build();
    }

    public EmailResponse getEmail(String email) {
        Optional<EmailEntity> emailEntityOptional = emailRepository.findByValue(email);
        EmailEntity emailEntity = emailEntityOptional.orElseThrow(() -> new GenericHttpException("There is no email record yet.", HttpStatus.NOT_FOUND));
        return emailEntityConverter.convert(emailEntity);
    }

    public void createEmail(EmailRequest emailRequest) {
        EmailEntity emailEntity = EmailEntity.builder().value(emailRequest.getEmail()).build();
        boolean isEmailExists = emailRepository.exists(Example.of(emailEntity));
        if (!isEmailExists) {
            Integer count = 1;
            if (Objects.nonNull(emailRequest.getCount())) {
                count = emailRequest.getCount();
            }
            emailEntity.setTotalCount(count);
            emailRepository.save(emailEntity);
        } else {
            throw new GenericHttpException("There is an email record, you may only update that record.", HttpStatus.CONFLICT);
        }
    }

    public void updateEmail(EmailRequest emailRequest) {
        Optional<EmailEntity> emailEntityOptional = emailRepository.findByValue(emailRequest.getEmail());
        if (emailEntityOptional.isPresent()) {
            EmailEntity emailEntity = emailEntityOptional.get();
            emailEntity.setValue(emailRequest.getEmail());
            emailEntity.setTotalCount(emailRequest.getCount());
            emailRepository.save(emailEntity);
        } else {
            throw new GenericHttpException("There is no email record to update.", HttpStatus.METHOD_NOT_ALLOWED);
        }
    }

    public void deleteEmail(EmailRequest emailRequest) {
        Optional<EmailEntity> emailEntityOptional = emailRepository.findByValue(emailRequest.getEmail());
        if (emailEntityOptional.isPresent()) {
            emailRepository.delete(emailEntityOptional.get());
        } else {
            throw new GenericHttpException("There is no email record to delete.", HttpStatus.METHOD_NOT_ALLOWED);
        }
    }
}
