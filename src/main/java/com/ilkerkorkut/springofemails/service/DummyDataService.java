package com.ilkerkorkut.springofemails.service;

import com.ilkerkorkut.springofemails.model.Dataset;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class DummyDataService {

    @Value("${server.port}")
    private Integer serverPort;

    public Dataset createDummyData() {
        return Dataset.builder()
                .emails(generateEmailList())
                .resources(generateUrlList())
                .build();

    }

    private List<String> generateEmailList() {
        List<String> emailList = new ArrayList<>();
        int randomSize = generateRandomSize(1, 100);

        for (int i = 0; i < randomSize; i++) {
            boolean isValidEmail = generateRandomBoolean();
            if (isValidEmail) {
                emailList.add("user" + i + "@comeon.com");
            } else {
                emailList.add("user" + i + "@notvalid.com");
            }
        }
        return emailList;
    }

    private List<String> generateUrlList() {
        int randomSize = generateRandomSize(0, 1);
        List<String> urlList = new ArrayList<>();
        if (randomSize != 0) {
            for (int i = 0; i < randomSize; i++) {
                urlList.add("http://localhost:" + serverPort + "/data/feeder");
            }
        }
        return urlList;
    }

    private int generateRandomSize(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    private boolean generateRandomBoolean() {
        Random rd = new Random();
        return rd.nextBoolean();
    }
}
