package com.ilkerkorkut.springofemails.service;

import com.ilkerkorkut.springofemails.model.Dataset;
import com.ilkerkorkut.springofemails.util.ValidationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class DatasetProducerService {

    private final ProducerService producerService;

    @Value("${kafka.emails-topic}")
    private String emailsTopic;

    @Value("${kafka.urls-topic}")
    private String urlsTopic;

    @Async
    public void produceDataset(Dataset dataset) {
        if (Objects.isNull(dataset)) {
            return;
        }
        dataset.getResources().forEach(url -> {
            if (ValidationUtil.isValidUrl(url)) {
                producerService.sendMessage(urlsTopic, url);
            }
        });
        dataset.getEmails().forEach(email -> {
            if (ValidationUtil.isValidEmail(email)) {
                producerService.sendMessage(emailsTopic, email);
            }
        });
    }

}
