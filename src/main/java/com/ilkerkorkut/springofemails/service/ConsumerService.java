package com.ilkerkorkut.springofemails.service;


import com.ilkerkorkut.springofemails.client.ExternalDatasetClient;
import com.ilkerkorkut.springofemails.model.Dataset;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public final class ConsumerService {

    private final EmailService emailService;
    private final ExternalDatasetClient externalDatasetClient;
    private final DatasetProducerService datasetProducerService;

    @KafkaListener(topics = "${kafka.emails-topic}", groupId = "emails_group", containerFactory = "kafkaListenerMultiContainerFactory")
    public void consumeEmails(String email) {
        log.debug(String.format("=> Consumed email: %s", email));
        emailService.addToCache(email);
    }

    @KafkaListener(topics = "${kafka.urls-topic}", groupId = "urls_group", containerFactory = "kafkaListenerSingleContainerFactory")
    public void consumeUrls(String url) {
        log.info(String.format("=> Consumed url: %s", url));
        Dataset dataset = externalDatasetClient.getDataset(url);
        datasetProducerService.produceDataset(dataset);
    }
}
