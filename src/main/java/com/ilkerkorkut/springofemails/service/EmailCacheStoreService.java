package com.ilkerkorkut.springofemails.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.MapLoaderLifecycleSupport;
import com.hazelcast.map.MapStore;
import com.hazelcast.spring.context.SpringAware;
import com.ilkerkorkut.springofemails.persistence.EmailEntity;
import com.ilkerkorkut.springofemails.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

@SpringAware
public class EmailCacheStoreService implements MapStore<String, Integer>, MapLoaderLifecycleSupport {

    @Autowired
    private EmailRepository emailRepository;

    @Override
    public void init(HazelcastInstance hazelcastInstance, Properties properties, String mapName) {
        hazelcastInstance.getConfig().getManagedContext().initialize(this);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void store(String key, Integer value) {
        Optional<EmailEntity> emailEntityOptional = emailRepository.findByValue(key);
        if (emailEntityOptional.isPresent()) {
            EmailEntity emailEntity = emailEntityOptional.get();
            if (!emailEntity.getTotalCount().equals(value)) {
                emailEntity.setTotalCount(emailEntity.getTotalCount() + value);
            } else {
                emailEntity.setTotalCount(value);
            }
            emailRepository.save(emailEntity);
        } else {
            emailRepository.save(EmailEntity.builder()
                    .value(key)
                    .totalCount(value)
                    .build());
        }
    }

    @Override
    public void storeAll(Map<String, Integer> map) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            Optional<EmailEntity> emailEntityOptional = emailRepository.findByValue(entry.getKey());
            if (emailEntityOptional.isPresent()) {
                EmailEntity emailEntity = emailEntityOptional.get();
                if (!emailEntity.getTotalCount().equals(entry.getValue())) {
                    emailEntity.setTotalCount(emailEntity.getTotalCount() + entry.getValue());
                } else {
                    emailEntity.setTotalCount(entry.getValue());
                }
                emailRepository.save(emailEntity);
            } else {
                emailRepository.save(EmailEntity.builder()
                        .value(entry.getKey())
                        .totalCount(entry.getValue())
                        .build());
            }
        }
    }

    @Override
    public void delete(String key) {
    }

    @Override
    public void deleteAll(Collection<String> keys) {
    }

    @Override
    public Integer load(String key) {
        return null;
    }

    @Override
    public Map<String, Integer> loadAll(Collection<String> keys) {
        return null;
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return null;
    }
}
