package com.ilkerkorkut.springofemails.model.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
@AllArgsConstructor
public class GenericHttpException extends RuntimeException {

    private final String key;
    private final HttpStatus httpStatus;
}

