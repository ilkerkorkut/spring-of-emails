package com.ilkerkorkut.springofemails.model;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "dataset")
@XmlAccessorType(XmlAccessType.FIELD)
public class Dataset {

    @XmlElementWrapper(name = "emails")
    @XmlElement(name = "email")
    private List<String> emails;

    @XmlElementWrapper(name = "resources")
    @XmlElement(name = "url")
    private List<String> resources;


    public Dataset(List<String> emails, List<String> resources) {
        this.emails = emails;
        this.resources = resources;
    }

    public Dataset() {
    }

    public static DatasetBuilder builder() {
        return new DatasetBuilder();
    }

    public List<String> getEmails() {
        return this.emails;
    }

    public List<String> getResources() {
        return this.resources;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public void setResources(List<String> resources) {
        this.resources = resources;
    }

    public static class DatasetBuilder {
        private List<String> emails;
        private List<String> resources;

        DatasetBuilder() {
        }

        public Dataset.DatasetBuilder emails(List<String> emails) {
            this.emails = emails;
            return this;
        }

        public Dataset.DatasetBuilder resources(List<String> resources) {
            this.resources = resources;
            return this;
        }

        public Dataset build() {
            return new Dataset(emails, resources);
        }

        public String toString() {
            return "Dataset.DatasetBuilder(emails=" + this.emails + ", resources=" + this.resources + ")";
        }
    }
}
