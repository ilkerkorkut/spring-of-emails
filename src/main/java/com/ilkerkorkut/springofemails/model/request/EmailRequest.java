package com.ilkerkorkut.springofemails.model.request;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "emailRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailRequest {

    private String email;
    private Integer count;
}
