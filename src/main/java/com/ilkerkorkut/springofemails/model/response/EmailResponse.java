package com.ilkerkorkut.springofemails.model.response;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailResponse {

    private String email;
    private Integer count;
}
