package com.ilkerkorkut.springofemails.model.response;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

@XmlRootElement(name = "emailList")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailListResponse {

    @XmlElement(name = "email")
    private List<EmailResponse> emails;
}
