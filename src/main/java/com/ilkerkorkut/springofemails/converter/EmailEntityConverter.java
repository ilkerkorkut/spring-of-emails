package com.ilkerkorkut.springofemails.converter;

import com.ilkerkorkut.springofemails.model.response.EmailResponse;
import com.ilkerkorkut.springofemails.persistence.EmailEntity;
import org.springframework.stereotype.Component;

@Component
public class EmailEntityConverter {

    public EmailResponse convert(EmailEntity emailEntity) {
        return EmailResponse.builder()
                .email(emailEntity.getValue())
                .count(emailEntity.getTotalCount())
                .build();
    }
}
