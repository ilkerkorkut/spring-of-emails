package com.ilkerkorkut.springofemails.client;

import com.ilkerkorkut.springofemails.configuration.HttpClientConfiguration;
import com.ilkerkorkut.springofemails.model.Dataset;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.util.retry.Retry;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class ExternalDatasetClient {

    private final HttpClientConfiguration httpClientConfiguration;

    private WebClient webClient;

    @PostConstruct
    public void initWebClient() {
        HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, httpClientConfiguration.getConnectionTimeoutMillis())
                .responseTimeout(Duration.ofMillis(httpClientConfiguration.getResponseTimeoutMillis()))
                .doOnConnected(conn ->
                        conn.addHandlerLast(new ReadTimeoutHandler(httpClientConfiguration.getReadTimeoutMillis(), TimeUnit.MILLISECONDS))
                                .addHandlerLast(new WriteTimeoutHandler(httpClientConfiguration.getWriteTimeoutMillis(), TimeUnit.MILLISECONDS)));

        this.webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();
    }

    public Dataset getDataset(String url) {
        return webClient.get()
                .uri(url)
                .accept(MediaType.APPLICATION_XML)
                .retrieve()
                .bodyToMono(Dataset.class)
                .retryWhen(Retry.backoff(3, Duration.ofSeconds(1)))
                .block();
    }
}
