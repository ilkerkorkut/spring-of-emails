package com.ilkerkorkut.springofemails.persistence;

import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "emails", schema = "springemail")
public class EmailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "value", unique = true)
    private String value;

    @Column(name = "total_count")
    private Integer totalCount;

}
